<?php

get_header(); ?>


<div class="container">
	<div class="blog-heading">
		<h1>Blogs</h1>
	</div>
	<div class="row">
		<div class="col col-lg-8">
	<?php 
	global $paged;

	$arg=array(
		'paged'=>$paged,
		'category_name'     =>  'blog',
        // 'tag'               =>  $flr_tag, 
        'posts_per_page'    =>  2,
		);

	$args = new WP_Query($arg);
		if($args->have_posts()):

			while($args->have_posts()): $args->the_post();?>
					<div class="blog-title">
							<?php the_title(sprintf('<h3 class="entry-title"><a href="%s" rel="bookmark">',  esc_url(get_permalink())), '</a></h3>'); ?>
			</div>
			<?php get_template_part( 'content',get_post_format()); ?>
					<div class="blog-desc">
				<?php echo excerpt(30);?>
				<a href="<?php the_permalink();?>">Read More</a>
			</div>

					<?php 
			endwhile;

?>
<?php
 // $big = 999999999; // need an unlikely integer

echo paginate_links( array(
    // 'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, $paged),
    'total' => $args->max_num_pages,
) );
  ?>

<?php 		endif;


	 ?>



<!-- <?php 
// endforeach; 

?>
 -->

 
</div>



	<div class="col col-lg-4">
			<div class="search">
				<?php get_search_form();?>	
			</div>

		<div class="recent-post">
			<!--Recent Post  -->

			<h5>Recent Posts</h5>
			<ul>
			<?php
			$recent_posts = wp_get_recent_posts();
			foreach ($recent_posts as $recent) {
				echo '<li><a href="'.get_permalink($recent["ID"]).'">'.$recent["post_title"].'</a> </li> ';
			}
			wp_reset_query();
			?>
			</ul>
			
			<!-- End recent Post -->
		</div>

		<div class="archieves">
			<h5>Archieves</h5>
			<?php wp_get_archives();?>
		</div>
		
	</div>	

</div>


</div>
<?php

get_footer();?>