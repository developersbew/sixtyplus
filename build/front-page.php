<?php get_header();?>


    <div class="section-home-organisation-desc section">
        <div class="container">
            <div class="row">
                <div class="col col-lg-6">
                    <div class="home-intro-image">
                        <div class="inner-wrap">
                            
                        </div>
                    </div>
                </div>
                <div class="col col-lg-6">
                    <div class="home-intro-desc">
                        <div class="inner-wrap">
                            <h2 class="sub-title">Explore Circles</h2>
                            <p class="desc">
                                <span class="bold">Lean In Circles</span> are small groups who meet regularly to learn and grow together, and they’re changing lives. A Circle can be a monthly roundtable at your 
                                home, a brown-bag lunch series at work, or even a virtual meet-up with people from around the world.
                                </p>
                                <p class="desc">Lean In Circles are small groups who meet regularly to learn and grow together, and they’re changing lives. A Circle can be a monthly roundtable at your 
                                home, a brown-bag lunch series at work, or even a virtual meet-up with people from around the world.
                                Lean In Circles are small groups who meet regularly to learn and grow together, and they’re changing lives. A Circle can be a monthly roundtable at your 
                                home
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-home-message">
        <h2 class="title">Lets meet and have a Coffee !!</h2>
    </div>



    <div class="section-home-features section">
        <div class="container">
        <h2 class="title">Features</h2>
            <div class="row no-gutter">
                <div class="col-lg-4 col">
                    <div class="feature-bg"></div>
                    <div class="content">
                        <h4 class="sub-title">Title</h4>
                        <p class="desc">hsdkf ksdfds jsdsdf ksdsdg sdkgfdkf kjfsdgdsk dh dgfds dsigfd fdds ldds  lisdgfl sdf</p>
                         <a href="#">know more</a> 
                    </div>
                </div>
                <div class="col-lg-4 col">
                    <div class="feature-bg"></div>
                    <div class="content">
                        <h4  class="sub-title">Title</h4>
                        <p class="desc">hsdkf ksdfds jsdsdf ksdsdg sdkgfdkf kjfsdgdsk dh dgfds dsigfd fdds ldds  lisdgfl sdf</p>
                         <a href="#">know more</a> 
                    </div>
                </div>
                <div class="col-lg-4 col">
                    <div class="feature-bg"></div>
                    <div class="content">
                        <h4  class="sub-title">Title</h4>
                        <p class="desc">hsdkf ksdfds jsdsdf ksdsdg sdkgfdkf kjfsdgdsk dh dgfds dsigfd fdds ldds  lisdgfl sdf</p>
                         <a href="#">know more</a> 
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="section-home-contact section">
        <div class="container">
            <div class="wrap">
                <span class="sub-title">Follow Us On</span>
                <ul>
                    <li><a>facebook</a></li>
                    <li><a>twitter</a></li>
                    <li><a>youtube</a></li>
                </ul>
            </div>
        </div>
             
    </div>
       

    <div class="section-home-blog section">
        <div class="container">
            <h2 class="title"><a href="index.php/blog">Blogs</a></h2>
            <div class="row no-gutters">
                <?php 
                $args = array('posts_per_page' => 4, 'category_name' => 'blog');
                $myposts = get_posts($args);
                foreach ($myposts as $post):
                setup_postdata($post);
                ?>
                <div class="col-lg-3">
                    
                        <article class="home-blogs">
                            <h3 class="sub-title"><?php the_title(); ?> </h3>
                            <hr>
                            <p class="blog-summary desc"> <?php echo excerpt(15)?></p>
                            <a class="read-more" href="<?php the_permalink(); ?>"> Read more </a>
                        </article>
                    
                </div>
                <?php wp_reset_postdata();
                endforeach;?>
             </div>
        </div>
    </div>

<?php get_footer(); ?>



            