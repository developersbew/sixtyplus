<?php
/**
 * sixty plus functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sixty_plus
 */

if ( ! function_exists( 'sixty_plus_setup' ) ) :
	
	function sixty_plus_setup() {
		
		load_theme_textdomain( 'sixty_plus', get_template_directory() . '/languages' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array(
			'header_links' => esc_html__( 'Primary', 'sixty_plus' ),
			'footer_links' => esc_html__( 'Secondary', 'sixty_plus' ),
		) );

	
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		
 	}
 endif;

add_action( 'after_setup_theme', 'sixty_plus_setup' );


function sixty_plus_scripts() {

	wp_enqueue_style( 'sixty_plus-style', get_template_directory_uri().'/css/style.css' );
	wp_enqueue_style( 'sixty_plus_fonts', 'https://fonts.googleapis.com/css?family=Bitter:400,700|Libre+Franklin:300,400,500,600' );
	wp_enqueue_script( 'site_wide_js', get_template_directory_uri() . '/js/sixty_plus.min.js', array(),true );

}
add_action( 'wp_enqueue_scripts', 'sixty_plus_scripts' );

function sixty_plus_slides() {

	$labels = array(
		"name" => ( 'Slides' ),
		"singular_name" => ( 'slide'),
	);

	$args = array(
		"label" => ( 'Slider'),
		"labels" => $labels,
		"description" => "Add New Slides",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "slider", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "page-attributes"),
	);

	register_post_type( "slide", $args );
}

add_action( 'init', 'sixty_plus_slides' );



function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}



