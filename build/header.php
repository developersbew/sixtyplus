<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sixty_plus
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body>

<div class="big-container">
    <div class="header-container">
        <div class="logo-container">
            <div class="logo">
                <a href=""></a>
            </div>
        </div>

		<?php if ( is_front_page() ) : ?>

        <div class="slider">  
            <div class="slider-contents">
                <ul class="slide-container">

                    <?php 
                        $args = array(
                            'post_type'=> 'slide',
                            'order'    => 'ASC'
                        ); 
                        $the_query = new WP_Query( $args );
                        if($the_query->have_posts() ) : 
                            while ( $the_query->have_posts() ) :
                                $the_query->the_post();
                    ?> 
                 
                    <li style="background:url('<?php echo get_the_post_thumbnail_url(); ?>') no-repeat 50%;background-size:cover;">
                      <div class="container">
                        <div class="slides">
                          <h2><?php the_title() ?></h2>
                          <?php the_content()?>
                        </div>
                      </div>
                    </li>

                    <?php
                       endwhile;
                        endif; 
                    ?>
                    
                  </ul>
              
            </div>
          </div>

			<?php
			endif;

				?>

                <header data-header-fix>
                    <div class="container">
                        <nav id="site-navigation" class="main-navigation">
                            <?php
                              wp_nav_menu( array(
                              'theme_location' => 'primary',
                                ) );
                            ?>
                        </nav>
                        <span>
                            <a href="" class="number">+919821363268</a>
                        </span>
                    </div>
                </header>

	<div id="content" class="site-content">


</div>
        <main>