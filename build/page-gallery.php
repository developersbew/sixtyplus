<?php get_header(); ?><br>


<div class="container">
	<h2>Gallery</h2>

<div class="row">
	<div class="col">
		<b>Image</b>

		<?php 
$args = array('post_per_page' => 5, 'category_name' => 'pic');

$myposts = get_posts($args);

foreach ($myposts as $post):
setup_postdata($post);?>
<a href="<?php the_permalink();?>"><?php the_content(); ?></a>
<?php 
endforeach;
wp_reset_postdata();

 ?>
	</div>

	<div class="col">
		<b>Audio</b>
			<?php 
$args = array('post_per_page' => 4, 'category_name' => 'audio');

$myposts = get_posts($args);

foreach ($myposts as $post):
setup_postdata($post);
// the_title();
the_content();
endforeach;
wp_reset_postdata();

 ?>
	</div>

	<div class="col">
		<b>Video</b>
		<iframe width="560" height="315" src="https://www.youtube.com/embed/XpV8EPTJsp4" frameborder="0" allowfullscreen></iframe>
	</div>
</div>

</div>

<?php get_footer(); ?>