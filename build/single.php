<?php
get_header();?><br>
<?php while (have_posts()):
the_post();?>


<?php
if (is_single()):?>
<div class="container">
		
		<?php get_search_form();?>

		<?php the_title('<h1 class="entry-title">', '</h1>');?><br><?php 
		 else :
		the_title(sprintf('<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>');
		endif;
		?>
			<?php the_post_thumbnail(); ?>
		<?php echo the_content();?>

		<?php
		endwhile;
		?>



	<div class="row">
	<div class="col">
		<?php if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
		?></div>

		<div class="col col-lg-4">
		<!--Recent Post  -->
		<?php if (!is_home()):?>
		<h2>Recent Posts</h2>
		<ul>
		<?php
		$recent_posts = wp_get_recent_posts();
		foreach ($recent_posts as $recent) {
			echo '<li><a href="'.get_permalink($recent["ID"]).'">'.$recent["post_title"].'</a> </li> ';
		}
		wp_reset_query();
		?>
		</ul>
		<?php endif;?>
		<h2>Recent Archieves</h2>
		<?php wp_get_archives();?>
		</div>
	</div>





</div>
<?php

//echo display_images_from_media_library();
get_footer();

// the_content();
?>
