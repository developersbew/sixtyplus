
'use strict';

///////////////////////////////////////////////////
////
////   FRONT END AUTOMATION
////
//////////////////////////////////////////////////


var gulp = require('gulp');
var watch=require('gulp-watch');
var browsersync = require('browser-sync').create();
var del=require('del');
var removeunusedcss=require('purify-css');
/////Plugin for requiring all the plugins automatically
var $ = require('gulp-load-plugins')({
	rename:{
		'gulp-sass':'sass',
		'gulp-plumber':'check',
		'gulp-clean-css':'cssmini',
		'gulp-concat':'concat',
		'gulp-autoprefixer':'cssprefix',
		'gulp-jshint':'checkjs',
		'gulp-uglify':'jsmini',

	}
});



///////////////////////////////////////
// Styles Task
///////////////////////////////////////
gulp.task('styles',function(){
	return gulp.src(['sixty_plus/scss/style.scss','sixty_plus/bower_components/bootstrap/dist/css/bootstrap.min.css'])
	.pipe($.check())
	.pipe($.sass())
	.pipe($.cssprefix({browsers:['last 2 versions']}))
	.pipe($.cssmini())
	.pipe($.concat('style.css'))
	.pipe(gulp.dest('sixty_plus/css'))
	.pipe(browsersync.stream());

});

///////////////////////////////////////
// JS Task
///////////////////////////////////////
gulp.task('scripts',function(){
	return gulp.src(['sixty_plus/bower_components/jquery/dist/jquery.js','sixty_plus/bower_components/typed.js/js/typed.js','sixty_plus/bower_components/tether/dist/js/tether.js','sixty_plus/bower_components/bootstrap/dist/js/bootstrap.js','sixty_plus/js/vendor/modernizr.js','sixty_plus/js/vendor/easing.js','sixty_plus/bower_components/flexslider/jquery.flexslider-min.js','sixty_plus/js/**/*.js','!sixty_plus/js/*.min.js'])
	.pipe($.check())
	.pipe($.checkjs())
	.pipe($.checkjs.reporter('default'))
	.pipe($.concat('sixty_plus.min.js'))
	.pipe($.jsmini())
	.pipe(gulp.dest('sixty_plus/js'))
	.pipe(browsersync.stream());
});

///////////////////////////////////////
// html Task
///////////////////////////////////////
gulp.task('html',function(){
	return gulp.src('sixty_plus/**/*.html')
	.pipe(browsersync.stream());
});

///////////////////////////////////////
// php Task
///////////////////////////////////////
gulp.task('php',function(){
	return gulp.src('sixty_plus/**/*.php')
	.pipe(browsersync.stream());
});

///////////////////////////////////////
// Watch Task
///////////////////////////////////////
gulp.task('watch',function(){
	 gulp.watch('sixty_plus/js/main.js',['scripts']);
	 gulp.watch('sixty_plus/scss/style.scss',['styles']);
	 gulp.watch('sixty_plus/**/*.html',['html']);
	 gulp.watch('sixty_plus/**/*.php',['php']);
});


///////////////////////////////////////
// Browser Sync
///////////////////////////////////////
gulp.task('browser-sync',function(){
	browsersync.init({
		proxy: "local.dev"
	});
});

/*gulp.task('build:serve',function(){
	browsersync.init({
		server:{
			baseDir:"./build/"
		}
	});
});*/

//////////////////////////////////
// FINAL PRODUCT BUILD
/////////////////////////////////




gulp.task('build:copy',function(){
	return gulp.src('sixty_plus/**/*/')
	.pipe(gulp.dest('build/'));
})

gulp.task('build:removeextra',['build:copy'],function(){
	var content=['build/js/main.js','build/**/*.php','build/**/*.html'];
	var css=['build/css/style.css'];
	var options={
		output:'build/css/style.css',
		minify: true,
		rejected: true,
		whitelist:['*jumbo-slider*','*latest-news*']
	};
	return removeunusedcss(content,css,options);
})

gulp.task('build:remove',['build:removeextra'],function(cb){
	del([
		'build/scss/',
		'build/bower_components/',
		'build/js/!(*.min.js)'
		],cb);
});

gulp.task('build',['build:copy','build:remove']);

///////////////////////////////////////
// Default Task
///////////////////////////////////////
gulp.task('default',['styles','scripts','browser-sync','watch']);


